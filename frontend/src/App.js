import './App.css';
import { useState,useEffect} from 'react';
import OperationButton from './components/OperationButton';
import SizeMatrix from './components/SizeMatrix';
import MatrixInterface from './components/MatrixInterface';
import AdditionalComponent from './components/AdditionalComponent';
import OutputMatrix from './components/OutputMatrix'
function App() {
  const [matrixSize, setMatrixSize] = useState({
    rows: 2,
    columns: 2,
  })
  const [matrix, setMatrix] = useState([[0, 0], [0, 0]])
  
  const [status, setStatus] = useState(1)
  const [q, setQ] = useState(2)
  const [answer, setAnswer] = useState([])
  
  return (
    <div className="App">
      <h1 className='Head'>Введение в теорию кодирования</h1>
      <div className='Menu'>
        <OperationButton setStatus= {status => setStatus(status)} status = {status} id = {1} name="Поиск порождающей матрицы"/>
        <OperationButton setStatus= {status => setStatus(status)} status = {status} id = {2} name="Поиск проверочной матрицы" />
        <OperationButton setStatus= {status => setStatus(status)} status = {status} id = {3} name="Что-то" />
      </div>
        <div className='AddMenu'>
            <p>Дополнительное Меню:</p>
            <AdditionalComponent description="Модуль: " default={2} function={q=>setQ(q)}/>
        </div>
        <div>Введите размерность</div>
        <SizeMatrix setMatrixSize={object => setMatrixSize(object)}/>
        <div>Матрица</div>
        <MatrixInterface setAnswer={setAnswer} q={q} status={status} matrixSize={matrixSize} setMatrix={matrix => setMatrix(matrix)}/>
      <p>Вывод:</p>
        {/*<output>*/}
        {/*    {answer}*/}
        {/*</output>*/}
        <OutputMatrix answer={answer}/>

      
    </div>
  );
}

export default App;

import * as React from 'react';
import './Mat.css';
import axios from 'axios';

const MatrixInterface =  function (props) {
    let matrix = Array(props.matrixSize.rows)
    for (let i = 0; i < props.matrixSize.rows; i++) {
        matrix[i] = new Array(props.matrixSize.columns).fill(0)
        
    }
    const handleSubmit = event => {
      event.preventDefault();
      let count = 0;
      for (let i = 0; i < props.matrixSize.rows; i++) {
        for (let j = 0; j < props.matrixSize.columns; j++) {
          matrix[i][j] = !isNaN(parseInt(event.target[count].value)) ? parseInt(event.target[count].value) : 0;
          count += 1;
        }
        
      }
      props.setMatrix(matrix);  
      console.log(matrix);

      
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        matrix: matrix,
        action: props.status,
        q: props.q
      };
    console.log(requestOptions)
    axios.post('http://62.84.120.70:8181', requestOptions)
    .then(function (response) {
     props.setAnswer(response.data.matrix);
     console.log(response)
    })
    .catch(function (error) {
      console.log(error);
    });
  
    }
    return (
      <form onSubmit = {handleSubmit}>
      {matrix.map((row, indexRow = 1) => {
        return (<div key={indexRow}>
                {row.map((item, indexColumn = 1)=>{
                    return (
                <input
                  className='Inputclass'
                  key = {indexRow + " " + indexColumn}
                  type="number"
                  defaultValue={0}
                  name={indexRow + "," + indexColumn}
                />)})}
                </div>
        )
      })}
      <button>Посчитать</button>
    </form>
    );
}
export default MatrixInterface
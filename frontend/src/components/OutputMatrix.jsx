import * as React from "react";

const  OutputMatrix =  function (props)  {
    return (
        <div>
            {props.answer.map((row, i=1) => (
                <div key={i}>
                    {row.map((col, j) => (
                        <span key={j}>{col}</span>
                    ))}
                </div>
            ))}
        </div>

    );
}
export default OutputMatrix
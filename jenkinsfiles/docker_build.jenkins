#!groovy
// Run docker build
properties([disableConcurrentBuilds()])

pipeline { 
    agent any
    options {
        buildDiscarder(logRotator(numToKeepStr: '2', artifactNumToKeepStr: '2'))
        timestamps()
    }
    tools { 
        maven 'maven-3.8.5' 
        jdk 'openjdk-11' 
    }
    stages {
        stage ('Initialize') {
            steps {
                sh '''
                    echo "PATH = ${PATH}"
                    echo "M2_HOME = ${M2_HOME}"
                ''' 
            }
        }
        stage ('Build mvn') {
            when {
                anyOf {
                    changeset "backend/**"
                    changeset "frontend/**"
                    changeset "jenkinsfiles/**"
                    changeset "pom.xml"
                }
            }
            steps {
                updateGitlabCommitStatus name: 'build', state: 'pending'
                echo  " ============== start building frontend =================="
                sh 'mvn clean install'
                echo  " ============== start building backend ==================="
                dir ('backend') {
                	sh 'mvn clean package spring-boot:repackage'
                }
            }
            post {
                success {
                    updateGitlabCommitStatus name: 'build', state: 'success'
                    }
                failure {
                    updateGitlabCommitStatus name: 'build', state: 'failed'
                    }
            }
        }
        stage("create and run docker image") {
            when {
                anyOf {
                    changeset "backend/**"
                    changeset "frontend/**"
                    changeset "dockerfiles/**"
                    changeset "jenkinsfiles/**"
                    changeset "pom.xml"
                }
            }
            steps {
                echo " ============== start building docker image =================="
                sh 'cp /var/lib/jenkins/workspace/docker_b/backend/target/some-app-backend-0.1-SNAPSHOT.jar /var/lib/jenkins/workspace/docker_b/dockerfiles/some-app-backend-0.1-SNAPSHOT.jar'
                dir ('dockerfiles') {
                    sh 'docker 2>/dev/null 1>&2 stop app || true'
                    sh 'docker 2>/dev/null 1>&2 rmi jarapp:release || true'
                	sh 'docker build -t jarapp:release . '
                }
            }
        }
        stage("run docker container") {
            when {
                anyOf {
                    changeset "backend/**"
                    changeset "frontend/**"
                    changeset "dockerfiles/**"
                    changeset "jenkinsfiles/**"
                    changeset "pom.xml"
                }
            }
            steps {
                echo " ============== run docker container ========================="
                sh 'docker run --rm --name app -d -p 8181:8181 jarapp:release'
                sh 'docker ps'
            }
        }


    }
}
